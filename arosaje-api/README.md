# Installation et configuration API Arosaje

## Initialisation du projet
`npm init`

##### Installation de yarn
`npm install --global yarn`

## Installation des packages nécéssaires

##### Utilitaire qui permet de redémarrer automatiquement le serveur lorsqu'un fichier est modifié
`yarn add nodemon`

##### Installation framework nodejs
`yarn add express` 

##### Installation du package pour la BDD
`yarn add pg` 

##### Installation de cors qui permet de ne plus rendre bloquant l'accès aux ressources via les requêtes
`yarn add cors`

##### Package pour l'upload de fichiers
`yarn add multer`

##### Installation fichier des variables d'environnement
`yarn add dotenv`

## Lancement de l'api
`nodemon ./server.js`

## Lancement des Tests

##### Installation de mocha
`npm install mocha -g `

##### Lancement des tests
`mocha -R spec <nomFichier>`

##### Faire tourner les tests 
`npm i`
