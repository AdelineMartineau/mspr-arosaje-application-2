const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const dotenv = require('dotenv')
const userRouter = require("./src/routes/userRoute")
const plantRouter = require("./src/routes/plantRoute")
const tipsRouter = require("./src/routes/tipsRoute")
const fileRouter = require('./src/routes/fileRoute')
const announcementRouter = require('./src/routes/announcementRoute')

dotenv.config();

const app = express()
const port = 3002

app.use(express.json())
app.use(cookieParser())
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.send("Hello world")
})

app.use("/api/users", userRouter)
app.use("/api/plants", plantRouter)
app.use("/api/tips", tipsRouter)
app.use("/api/files", fileRouter)
app.use("/api/announcements", announcementRouter)

app.listen(port, () => console.log(`Server is listening on port ${port}`))