const createAnnouncement = "INSERT INTO announcement (idOwner, idplant, title, description, startDate, endDate) VALUES ($1,$2,$3,$4,$5,$6);"
const checkAnnounceExist = "SELECT * FROM announcement where idOwner = $1 AND idplant = $2"

const getMyAnnounces = `SELECT announcement.id AS announcementId, announcement.title AS announcementtitle, announcement.description AS announcementdescription, announcement.startDate AS announcementstartdate, announcement.endDate AS announcementenddate, announcement.idplant AS announcementidplant, announcement.idKeeper AS announcementIdKeeper, announcement.idOwner AS announcementIdOwner, plant.id AS plantId, plant.libelle AS plantlibelle, plant.profilpicture AS plantprofilpicture, plant.description AS plantdescription, plant.place AS plantplace, plant.idplanttype AS plantidplanttype, plant.idOwner AS plantIdOwner, users.id AS userownerid, users.firstname AS userownerfirstname, users.lastname AS userownerlastname, users.email AS userowneremail, user2.id AS userKeeperId, user2.firstname AS userkeeperfirstname, user2.lastname AS userkeeperlastname, user2.email AS userkeeperemail 
                        FROM announcement 
                        INNER JOIN plant ON announcement.idplant = plant.id 
                        INNER JOIN users ON announcement.idOwner = users.id 
                        INNER JOIN users AS user2 ON announcement.idKeeper = user2.id 
                        WHERE announcement.idOwner = $1
                        GROUP BY announcement.id, plant.id, users.id, user2.id`

const getMyAnnouncesPart2 = `SELECT announcement.id AS "announcementId", announcement.title AS "announcementtitle", announcement.description AS "announcementdescription",announcement.startDate AS "announcementstartdate",announcement.endDate AS "announcementenddate",announcement.idplant AS "announcementidplant",announcement.idKeeper AS "announcementIdKeeper",announcement.idOwner AS "announcementIdOwner",plant.id AS "plantId",plant.libelle AS "plantlibelle",plant.profilpicture AS "plantprofilpicture",plant.description AS "plantdescription",plant.place AS "plantplace",plant.idplanttype AS "plantidplanttype",plant.idOwner AS "plantIdOwner",users.id AS "userownerid",users.firstname AS "userownerfirstname",users.lastname AS "userownerlastname",users.email AS "userowneremail"
                            FROM announcement 
                            INNER JOIN plant ON announcement.idplant = plant.id 
                            INNER JOIN users ON announcement.idOwner = users.id 
                            WHERE announcement.idOwner = $1 AND announcement.idKeeper IS NULL
                            GROUP BY announcement.id, plant.id, users.id`

const getAllAnnounces = `SELECT announcement.id AS "announcementId", announcement.title AS "announcementtitle", announcement.description AS "announcementdescription", announcement.startDate AS "announcementstartdate", announcement.endDate AS "announcementenddate", announcement.idplant AS "announcementidplant", announcement.idKeeper AS "announcementIdKeeper", announcement.idOwner AS "announcementIdOwner", plant.id AS "plantId", plant.libelle AS "plantlibelle", plant.profilpicture AS "plantprofilpicture", plant.description AS "plantdescription", plant.place AS "plantplace", plant.idplanttype AS "plantidplanttype", plant.idOwner AS "plantIdOwner", users.id AS "userownerid", users.firstname AS "userownerfirstname", users.lastname AS "userownerlastname", users.email AS "userowneremail"
                        FROM announcement 
                        INNER JOIN plant ON announcement.idplant = plant.id 
                        INNER JOIN users ON announcement.idOwner = users.id 
                        WHERE announcement.idKeeper IS NULL
                        GROUP BY announcement.id, plant.id, users.id`

const getAnnouncesKeeped = `SELECT announcement.id AS "announcementId",announcement.title AS "announcementtitle",announcement.description AS "announcementdescription",announcement.startDate AS "announcementstartdate",announcement.endDate AS "announcementenddate",announcement.idplant AS "announcementidplant",announcement.idKeeper AS "announcementIdKeeper",announcement.idOwner AS "announcementIdOwner",plant.id AS "plantId",plant.libelle AS "plantlibelle",plant.profilpicture AS "plantprofilpicture",plant.description AS "plantdescription",plant.place AS "plantplace",plant.idplanttype AS "plantidplanttype",plant.idOwner AS "plantIdOwner",users.id AS "userownerid",users.firstname AS "userownerfirstname",users.lastname AS "userownerlastname",users.email AS "userowneremail",user2.id AS "userKeeperId",user2.firstname AS "userkeeperfirstname",user2.lastname AS "userkeeperlastname",user2.email AS "userkeeperemail"
                            FROM announcement 
                            INNER JOIN plant ON announcement.idplant = plant.id
                            INNER JOIN users ON announcement.idOwner = users.id
                            INNER JOIN users AS user2 ON announcement.idKeeper = user2.id
                            WHERE announcement.idKeeper = $1
                            GROUP BY announcement.id, plant.id, users.id, user2.id`

const keepPlant = 'UPDATE announcement SET idKeeper = $1 WHERE id = $2'
const stopKeepPlant = 'UPDATE announcement SET idKeeper = NULL WHERE idKeeper = $1 AND id = $2'

const deleteAnnounce = `DELETE FROM announcement WHERE id = $1 AND idOwner = $2 
                        INNER JOIN file on createAnnouncement.id = file.idAnnouncement`

module.exports = {
    createAnnouncement, 
    checkAnnounceExist,
    getMyAnnounces,
    getMyAnnouncesPart2,
    getAllAnnounces,
    getAnnouncesKeeped,
    keepPlant,
    stopKeepPlant,
    deleteAnnounce
}