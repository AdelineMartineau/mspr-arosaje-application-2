const getFilesByPlantId = "SELECT * FROM file WHERE idplant = $1"
const getFilesByAnnouncementAndPlantId = "SELECT * FROM file WHERE idplant = $1 AND idAnnouncement = $2"
const createFile = "INSERT INTO file (idAnnouncement, idplant, image, date) VALUES ($1, $2, $3, $4)"

module.exports = {
    getFilesByPlantId,
    getFilesByAnnouncementAndPlantId,
    createFile,
};