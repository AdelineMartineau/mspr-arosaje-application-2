const getAllTips = "SELECT * FROM tips INNER JOIN plant ON tips.idplant = plant.id WHERE plant.idOwner = $1 AND tips.idplant = $2"
const createTips = "INSERT INTO tips (idplant, comment) VALUES ($1, $2)"

module.exports = {
    getAllTips, 
    createTips
}