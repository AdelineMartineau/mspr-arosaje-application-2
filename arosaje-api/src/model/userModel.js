const getUsers = "SELECT * FROM users"
const getUserById = "SELECT * FROM users WHERE id = $1"
const getUserByEmail = "SELECT * from users where email = $1"
const updateUserById = "UPDATE users SET firstname = $1 WHERE id = $2"

module.exports = {
    getUsers, 
    getUserById,
    getUserByEmail,
    updateUserById,
}