const getAllPlants = "SELECT * FROM plant"
const getAllPlantsByUserId = "SELECT * FROM plant WHERE idOwner = $1"
const createPlant = "INSERT INTO plant (idplanttype, idOwner, libelle, profilpicture, description, place) VALUES ($1, $2, $3, $4, $5, $6)"
const getAllPlantType = "SELECT * FROM planttype"

module.exports = {
    getAllPlants, 
    getAllPlantsByUserId,
    createPlant,
    getAllPlantType
}