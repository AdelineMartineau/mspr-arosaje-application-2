const { Router } = require('express')
const service = require('../controller/tipsController')

const router = Router()

router.get("/", service.getAllTips)
router.post("/create", service.createTips)

module.exports = router

