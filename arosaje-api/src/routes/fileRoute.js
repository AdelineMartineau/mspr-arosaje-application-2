const { Router } = require('express')
const service = require('../controller/fileController')
const multer = require('multer');

const router = Router()
const upload = multer({ storage: multer.memoryStorage() });

router.get("/:idplant", service.getFilesByPlantId)
router.get("/:idAnnouncement/:idplant", service.getFilesByAnnouncementAndPlantId)
router.post("/create", upload.single('image'), service.createFile)

module.exports = router

