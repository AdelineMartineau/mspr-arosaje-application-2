const { Router } = require('express')
const service = require('../controller/plantController')
const multer = require('multer');

const router = Router()
const upload = multer({ storage: multer.memoryStorage() });

router.get("/", service.getAllPlants)
router.get("/type", service.getAllPlantType)
router.get("/:idUser", service.getAllPlantsByUserId)
router.post("/create", upload.single("profilPicture"), service.createPlant)

module.exports = router

