const { Router } = require('express')
const service = require('../controller/announcementController')

const router = Router()

router.post("/create", service.createAnnouncement)
router.post("/keepPlant", service.keepPlant)
router.post("/stopKeepPlant", service.stopKeepPlant)
router.delete("/delete", service.deleteAnnounce)
router.get("/myAnnounces/:idOwner", service.getMyAnnounces)
router.get("/myAnnouncesPart2/:idOwner", service.getMyAnnouncesPart2)
router.get("/announces", service.getAllAnnounces)
router.get("/announcesKeeped/:idKeeper", service.getAnnouncesKeeped)

module.exports = router

