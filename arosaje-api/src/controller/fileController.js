const pool = require('../../database')
const queries = require('../model/fileModel')

const getFilesByPlantId = (req, res) => {
    pool.query(queries.getFilesByPlantId, [req.params.idplant], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const getFilesByAnnouncementAndPlantId = (req, res) => {
    pool.query(queries.getFilesByAnnouncementAndPlantId, [req.params.idplant, req.params.idAnnouncement], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const createFile = async (req, res) => {
    try {
        const {idAnnouncement, idplant, date} = req.body
        const image = req.file.buffer;

        await pool.query(queries.createFile, [image, idAnnouncement, idplant, date])

        res.status(201).send("Fichier créé avec succès")
    } catch (error) {
        res.status(500).send({"error": error.message})
    }
}


module.exports = { 
    getFilesByPlantId,
    getFilesByAnnouncementAndPlantId,
    createFile
}