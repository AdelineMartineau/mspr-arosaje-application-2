const pool = require('../../database')
const queries = require('../model/tipsModel')


const getAllTips = (req, res) => {
    pool.query(queries.getAllTips, [req.query.idOwner, req.query.idplant], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}


const createTips = async (req, res) => {
    try {
        const {idplant, comment} = req.body

        await pool.query(queries.createTips, [idplant, comment])

        res.status(201).send("Votre conseil a bien été enregistré")
    } catch (error) {
        res.status(500).send({"error": error.message})
    }
}

module.exports = { 
    getAllTips,
    createTips,
}