const pool = require('../../database')
const queries = require('../model/plantModel')


const getAllPlants = (req, res) => {
    pool.query(queries.getAllPlants, (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const getAllPlantsByUserId = (req, res) => {
    pool.query(queries.getAllPlantsByUserId, [req.params.idUser], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const getAllPlantType = (req, res) => {
    pool.query(queries.getAllPlantType, (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const createPlant = async (req, res) => {
    try {
        const {idplanttype, idOwner, libelle, description, place} = req.body
        const profilpicture = req.file.buffer;  

        await pool.query(queries.createPlant, [idplanttype, idOwner, libelle, profilpicture, description, place])

        res.status(201).send("Plante créée avec succès")
    } catch (error) {
        res.status(500).send({"error": error.message})
    }
}

module.exports = { 
    getAllPlants,
    getAllPlantsByUserId,
    getAllPlantType,
    createPlant
}