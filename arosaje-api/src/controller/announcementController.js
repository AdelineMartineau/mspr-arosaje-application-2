const pool = require('../../database')
const queries = require('../model/annoucementModel')

const createAnnouncement = async (req, res) => {
    try {
        const {idOwner, idplant, title, description, startDate, endDate} = req.body
        const checkIsAlreadyExist = await pool.query(queries.checkAnnounceExist, [idOwner, idplant])

        if(checkIsAlreadyExist.rows.length) {
            return res.status(400).json({error : "Une annonce existe déjà pour cette plante dans les dates souhaitées."})
        }

        await pool.query(queries.createAnnouncement, [idOwner, idplant, title, description, startDate, endDate ])

        res.status(201).send("Annonce créée avec succès")
    } catch (error) {
        res.status(500).send({"error": error.message})
    }
}

const getMyAnnounces = (req, res) => {
    pool.query(queries.getMyAnnounces, [req.params.idOwner], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const getMyAnnouncesPart2 = (req, res) => {
    pool.query(queries.getMyAnnouncesPart2, [req.params.idOwner], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const getAllAnnounces = (req, res) => {
    pool.query(queries.getAllAnnounces, (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const getAnnouncesKeeped = (req, res) => {
    pool.query(queries.getAnnouncesKeeped, [req.params.idKeeper], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const keepPlant = (req, res) => {
    try {
        const {idConnectedUser, idAnnounce,} = req.body
        pool.query(queries.keepPlant, [idConnectedUser, idAnnounce])
        res.status(201).send("Votre garde à bien été enregistrée")
    } catch (error) {
        res.status(400).json({"error":error.message})
    }
}

const stopKeepPlant = (req, res) => {
    try {
        const {idKeeper, idAnnounce,} = req.body
        pool.query(queries.stopKeepPlant, [idKeeper, idAnnounce])
        res.status(201).send("Vous ne gardez plus cette plante")
    } catch (error) {
        res.status(400).json({"error":error.message})
    }
}

const deleteAnnounce = (req, res) => {
    try {
        const {idAnnounce, idConnectedUser} = req.body
        pool.query(queries.deleteAnnounce, [idAnnounce, idConnectedUser])
        res.status(201).send("L'annonce a bien été supprimée")
    } catch (error) {
        res.status(400).json({"error":error.message})
    }
}

module.exports = { 
    createAnnouncement,
    getMyAnnounces,
    getMyAnnouncesPart2,
    getAllAnnounces,
    getAnnouncesKeeped,
    keepPlant,
    stopKeepPlant,
    deleteAnnounce
}