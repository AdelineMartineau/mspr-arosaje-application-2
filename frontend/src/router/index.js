import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    redirect: {path: '/annonces'},
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '/profil/:idUser',
        name: 'Profil',
        component: () => import('@/components/Profil.vue'),
      },
      {
        path: '/listUsers',
        name: "listUsers",
        component: () =>import('@/components/ListUsers.vue'),
      },
      {
        path: '/annonces',
        name: 'Annonces',
        component: () => import('@/components/Annonces.vue'),
      },
      {
        path: '/mesAnnonces',
        name: "mesAnnonces",
        component: () => import('@/components/MesAnnonces.vue'),
      },
      {
        path: '/mesGardes',
        name: "mesGardes",
        component: () => import('@/components/MesGardes.vue'),
      },
      {
        path: '/privacyPolicy',
        name: "privacyPolicy",
        component: () => import('@/components/PrivacyPolicy.vue')
      },
      {
        path: '/login',
        name: 'Login',
        component: () => import('@/components/Login.vue'),
      },
      {
        path: '/inscription',
        name: 'Inscription',
        component: () => import('@/components/Inscription.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router