import Vuex from "vuex";
import announcementService from "@/services/announcementService";
import plantService from "@/services/plantService";

export default new Vuex.Store({
    state: {
        connectedUser: undefined,
        myAnnounces: [],
        allAnnounces: [],
        announcesThatIKeep: [],
        snackbar:{},
        userPlants: [],
        plantImages: []
    },
    mutations: {
        setConnectedUser(state, connectedUser) {
            state.connectedUser = connectedUser
        },
        setMyAnnounces(state, myAnnounces) {
            state.myAnnounces = myAnnounces
        },
        setAllAnnounces(state, allAnnounces) {
            state.allAnnounces = allAnnounces
        },
        setAnnouncesThatIKeep(state, announcesThatIKeep) {
            state.announcesThatIKeep = announcesThatIKeep
        },
        setSnackbar(state, snackbar) {
            state.snackbar = snackbar
        },
        setUserPlants(state, userPlants) {
            state.userPlants = userPlants
        },
        setPlantImages(state, plantImages) {
            state.plantImages = plantImages
        },
    },
    actions: {
        setConnectedUser({commit}, user) {
            commit("setConnectedUser", user)
        },
        fetchMyAnnounces({commit}, id) {
            announcementService.fetchMyAnnounces(id)
                .then((myAnnounces) => {
                    commit("setMyAnnounces", myAnnounces)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        fetchAllAnnounces({commit}) {
            announcementService.fetchAllAnnounces()
                .then((allAnnounces) => {
                    commit("setAllAnnounces", allAnnounces)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        setAnnouncesThatIKeep({commit}, announcesThatIKeep) {
            commit("setAnnouncesThatIKeep", announcesThatIKeep)
        },
        fetchAnnouncesThatIKeep({commit}, id) {
            announcementService.fetchAnnouncesThatIKeep(id)
                .then((announcesThatIKeep) => {
                    commit("setAnnouncesThatIKeep", announcesThatIKeep)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        setSnackbar({commit}, snackbar) {
            commit("setSnackbar", snackbar)
        },
        fetchUserPlants({commit}, id){
            plantService.fetchUserPlants(id)
                .then((plants) => {
                    commit("setUserPlants", plants)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        fetchPlantImages({commit}, obj) {
            plantService.fetchPlantImages(obj)
                .then((files) => {
                    commit("setPlantImages", files)
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }
})