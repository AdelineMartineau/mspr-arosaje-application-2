import App from './App.vue'
import { createApp } from 'vue'
import { registerPlugins } from '@/plugins'
import store from "./store/store";
import posthogPlugin from './plugins/posthog';
import { VueCookieNext } from 'vue-cookie-next'

const app = createApp(App)

registerPlugins(app)

app.use(posthogPlugin)
app.use(store)
app.use(VueCookieNext)
app.mount('#app')

VueCookieNext.config({ expire: '7d' })
