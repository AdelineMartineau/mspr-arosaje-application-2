export default {
    createFile: (formData) => {
        const requestOptions = {
            method: 'POST',
            mode: 'cors',
            redirect: 'follow',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        }
        return fetch(`http://localhost:3002/api/files/create`, requestOptions)
            .then((response) => response.json())
            .then((result, err) => {
                if (result.error) {
                    console.error(err);
                }
                return result
            });
    }
}