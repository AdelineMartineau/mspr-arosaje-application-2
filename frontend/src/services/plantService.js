export default {
    fetchUserPlants: (id) => {
        const requestOptions = {
            method: 'GET',
            mode: 'cors',
            redirect: 'follow',
        }
        return fetch(`http://localhost:3002/api/plants/${id}`, requestOptions)
            .then((response) => response.json())
            .then((result, err) => {
                if (result.error) {
                    console.error(err);
                }
                return result.map((plant) => {
                    if(!!plant.profilpicture){
                        const blob = new Blob([new Uint8Array(plant.profilpicture.data)], { type: plant.profilpicture.type });
                        const url = URL.createObjectURL(blob);
                        return {...plant, profilpicture: url}
                    } else {
                        return plant
                    }
                })
            });
    },
    fetchPlantImages: (obj) => {
        const requestOptions = {
            method: 'GET',
            mode: 'cors',
            redirect: 'follow',
        }
        if(!!obj.announcementId) {
            return fetch(`http://localhost:3002/api/files/?idplant=${obj.idplant}&idAnnouncement=${obj.announcementId }`, requestOptions)
                .then((response) => response.json())
                .then((result, err) => {
                    if (result.error) {
                        console.error(err);
                    }
                    return result.map((file) => {
                        if(!!file.image){
                            const blob = new Blob([new Uint8Array(file.image.data)], { type: file.image.type });
                            const url = URL.createObjectURL(blob);
                            return {...file, image: url}
                        } else {
                            return file
                        }
                    })
                });
        } else {
            return fetch(`http://localhost:3002/api/files/${obj.idplant}`, requestOptions)
                .then((response) => response.json())
                .then((result, err) => {
                    if (result.error) {
                        console.error(err);
                    }
                    return result.map((file) => {
                        if(!!file.image){
                            const blob = new Blob([new Uint8Array(file.image.data)], { type: file.image.type });
                            const url = URL.createObjectURL(blob);
                            return {...file, image: url}
                        } else {
                            return file
                        }
                    })
                });
        }
    }

}