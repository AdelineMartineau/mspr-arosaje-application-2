export default {
    fetchMyAnnounces: (id) => {
        const requestOptions = {
            method: 'GET',
            mode: 'cors',
            redirect: 'follow',
        }
        return fetch(`http://localhost:3002/api/announcements/myAnnounces/${id}`, requestOptions)
            .then((response) => response.json())
            .then((result, err) => {
                if (result.error) {
                    console.error(err);
                }
                const myAnnounces = result.map((myAnnounce) => {
                    if(!!myAnnounce.plantprofilpicture){
                        const blob = new Blob([new Uint8Array(myAnnounce.plantprofilpicture.data)], { type: myAnnounce.plantprofilpicture.type });
                        const url = URL.createObjectURL(blob);
                        return {...myAnnounce, plantprofilpicture: url}
                    } else {
                        return myAnnounce
                    }
                })
                return fetch(`http://localhost:3002/api/announcements/myAnnouncesPart2/${id}`, requestOptions)
                    .then((response) => response.json())
                    .then((resultPart2, err) => {
                        if (resultPart2.error) {
                            console.error(err);
                        }
                        const myAnnouncesPart2 = resultPart2.map((myAnnouncePar2) => {
                            if(!!myAnnouncePar2.plantprofilpicture){
                                const blob = new Blob([new Uint8Array(myAnnouncePar2.plantprofilpicture.data)], { type: myAnnouncePar2.plantprofilpicture.type });
                                const url = URL.createObjectURL(blob);
                                return {...myAnnouncePar2, plantprofilpicture: url}
                            } else {
                                return myAnnouncePar2
                            }
                        })
                        return [...myAnnouncesPart2, ...myAnnounces]
                    });
        })
    },
    fetchAllAnnounces: () => {
        const requestOptions = {
            method: 'GET',
            mode: 'cors',
            redirect: 'follow',
        }
        return fetch(`http://localhost:3002/api/announcements/announces`, requestOptions)
            .then((response) => response.json())
            .then((result, err) => {
                if (result.error) {
                    console.error(err);
                }
                return result.map((allAnnounce) => {
                    if(!!allAnnounce.plantprofilpicture){
                        const blob = new Blob([new Uint8Array(allAnnounce.plantprofilpicture.data)], { type: allAnnounce.plantprofilpicture.type });
                        const url = URL.createObjectURL(blob);
                        return {...allAnnounce, plantprofilpicture: url}
                    } else {
                        return allAnnounce
                    }
                })
            });
    },
    fetchAnnouncesThatIKeep: (id) => {
        const requestOptions = {
            method: 'GET',
            mode: 'cors',
            redirect: 'follow',
            }
        return fetch(`http://localhost:3002/api/announcements/announcesKeeped/${id}`, requestOptions)
            .then((response) => response.json())
            .then((result, err) => {
                if (result.error) {
                    console.error(err);
                }
                return result.map((announceThatIKeep) => {
                    if(!!announceThatIKeep.plantprofilpicture){
                        const blob = new Blob([new Uint8Array(announceThatIKeep.plantprofilpicture.data)], { type: announceThatIKeep.plantprofilpicture.type });
                        const url = URL.createObjectURL(blob);
                        return {...announceThatIKeep, plantprofilpicture: url}
                    } else {
                        return announceThatIKeep
                    }
                })
            });
    }
}