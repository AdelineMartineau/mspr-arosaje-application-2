import posthog from "posthog-js";

export default {
    install(app) {
        app.config.globalProperties.$posthog = posthog.init(
            "phc_qeMGa7IEm5LbUaKrPkig4I2nPxQQQhLHq0wfPhgT3Io",
            {
                api_host: "https://app.posthog.com"
            }
        )
    }
}