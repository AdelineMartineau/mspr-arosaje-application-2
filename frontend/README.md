# MSPR_AROSAJE FRONTEND

## Initialisation du projet

##### Installation de yarn
`npm install --global yarn` puis `yarn`

## Création du projet

##### Creation du projet avec ViteJs

`yarn create vite . --template vue`

##### Ajout de la librairie css Vuetify
`yarn add vuetify`

##### Installation vuex pour l'utilisation du store
`yarn add vuex@next --save`

##### Installation du router
`yarn add -D vue-router@4`

## Cookies et session

##### Etudier le comportement des utilisateurs du site web + cookie-banner
`yarn add posthog-js`

##### Gestion des cookies dans le navigateur
`yarn add vue-cookie-next`

## Compiles and hot-reloads for development
`yarn dev`

### Camera

##### Installation du module pour reconnaitre la camera de l'appareil
`yarn add webrtc-adapter`

## Customize configuration

See [Configuration Reference](https://vitejs.dev/config/).

### Liens utiles 

Installation de VueJS avec Vuetify : 

[https://next.vuetifyjs.com/en/getting-started/installation/](https://next.vuetifyjs.com/en/getting-started/installation/)

Bibliothèque Vuetify
[https://next.vuetifyjs.com/en/components/cards/](https://next.vuetifyjs.com/en/components/cards/)

VueX : https://vuex.vuejs.org/ 
