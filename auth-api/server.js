const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const dotenv = require('dotenv')
const usersRouter = require("./src/routes/usersRoute")
const authRouter = require("./src/routes/authRoute")

dotenv.config();

const app = express()
const port = 3001

app.use(express.json())
app.use(cookieParser())
app.use(cors())

app.get("/", (req, res) => {
    res.send("Hello world")
})

app.use("/api/users", usersRouter)
app.use("/api/auth", authRouter)

app.listen(port, () => console.log(`Server is listening on port ${port}`))