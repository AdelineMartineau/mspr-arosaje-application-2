const { Router } = require('express')
const service = require('../controller/userController')
const { authenticateToken } = require('../../middleware/authorization')

const router = Router()

router.get("/", authenticateToken, service.getUsers)
router.get("/:id", service.getUserById)
router.put("/:id", service.updateUserById)

module.exports = router
