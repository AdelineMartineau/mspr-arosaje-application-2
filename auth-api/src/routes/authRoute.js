const { Router } = require('express')
const authController = require('../controller/authController')

const router = Router()

router.post("/login", authController.login)
router.post("/create", authController.createUser)

module.exports = router
