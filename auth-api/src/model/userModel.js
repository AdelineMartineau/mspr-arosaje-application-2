const getUsers = "SELECT * FROM users"
const getUserById = "SELECT * FROM users WHERE id = $1"
const getUserByEmail = "SELECT * from users where email = $1"
const checkEmailExists = "SELECT email FROM users where email = $1"
const createUser = "INSERT INTO users (firstname, lastname, email, password) VALUES ($1, $2, $3, $4)"
const updateUserById = "UPDATE users SET firstname = $1 WHERE id = $2"
const login = "SELECT * FROM users WHERE email = $1 AND password = $2"

module.exports = {
    getUsers, 
    getUserById,
    getUserByEmail,
    checkEmailExists,
    createUser,
    updateUserById,
    login
}