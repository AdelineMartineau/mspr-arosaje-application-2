const pool = require('../../database')
const queries = require('../model/userModel')
const bcrypt = require('bcrypt')
const { jwtTokens } = require('../utils/jwt-helpers')

const login = async (req, res) => {
    try {
        const {email, password} = req.body
        const user = await pool.query(queries.getUserByEmail, [email])
        const validatePassword = await bcrypt.compare(password, user.rows[0].password)
        
        // Check password & email
        if(!user) return res.status(401).json({ error: "L'email est incorrect" })
        if(!validatePassword) return res.status(401).json({error: "Mot de passe incorrect" })

        // JWT
        var tokens = jwtTokens(user.rows[0]);
        res.cookie('refresh_token', tokens.refreshToken, { httpOnly: true })
        res.json({user: user.rows[0], tokens: tokens})

    } catch (error){
        res.status(401).json({ error:error.message })
    }
}

const createUser = async (req, res) => {
    try {
        const {firstname, lastname, email, password} = req.body
        const hashedPassword = await bcrypt.hash(password, 12)
        const checkEmail = await pool.query(queries.checkEmailExists, [email])

        if(checkEmail.rows.length) {
            return res.status(400).json({error : "L'email renseigné existe déjà"})
        }
        await pool.query(queries.createUser, [firstname, lastname, email, hashedPassword])

        res.status(201).send("Utilisateur créé avec succès")
    } catch (error) {
        res.status(500).json({"error":error.message})
    }
}

module.exports = { 
    login, 
    createUser,
}