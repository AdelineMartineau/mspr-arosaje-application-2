const pool = require('../../database')
const queries = require('../model/userModel')

const getUsers = (req, res) => {
    pool.query(queries.getUsers, (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const getUserById = (req, res) => {
    pool.query(queries.getUserById, [req.params.id], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(200).json(results.rows)
    })
}

const updateUserById = (req, res) => {
    const {firstname} = req.body
    pool.query(queries.getUserById, [req.params.id], (error, results) => {
        if (!results.rows.length){
            res.send("L'utilisateur n'existe pas")
        }

        pool.query(queries.updateUserById, [firstname, req.params.id], (error, results) => {
            if (error) {
                res.status(400).json({"error":error.message})
                return
            }
            res.status(201).send("Utilisateur modifier avec succès")
        })
    })
}

module.exports = { 
    getUsers, 
    getUserById, 
    updateUserById
}