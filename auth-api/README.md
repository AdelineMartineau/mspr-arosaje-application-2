# Installation et configuration API Authentification

## Initialisation du projet
`npm init`

##### Installation de yarn
`npm install --global yarn`

## Installation des packages nécéssaires

##### Installation framework nodejs
`yarn add express` 

##### Installation du package pour la BDD
`yarn add pg` 

##### Utilitaire qui permet de redémarrer automatiquement le serveur lorsqu'un fichier est modifié
`yarn add nodemon`

##### Installation de cors qui permet de ne plus rendre bloquant l'accès aux ressources via les requêtes
`yarn add cors`

##### Installation fichier des variables d'environnement
`yarn add dotenv`

## Authentification

##### Ajout d'un package de hashage
`yarn add bcrypt`

##### Ajout du package cookie-parser qui fournit un middleware pour l'analyse des cookies
`yarn add cookie-parser`

#####  Installation JWT pour l'authentification
`yarn add jsonwebtoken `

## Lancement de l'api
`nodemon ./server.js`

