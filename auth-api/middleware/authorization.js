const jwt = require('jsonwebtoken')
require('dotenv').config

const authenticateToken = (req, res, next) => {
    try{
        const queryToken = req.headers.authorization //Bearer TOKEN
        const accessToken = queryToken.split(' ')[1]
        const decodedToken =  jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET)

        const userId = decodedToken.user_id
        if (req.body.user_id && req.body.user_id !== userId) { 
            throw 'Invalid user ID';
        } else {
        next();
        }
    } catch (error){
        res.status(401).json({ message: error.message})
    }
}

module.exports = { authenticateToken }