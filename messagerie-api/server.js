const WebSocket = require('ws');
const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const dotenv = require('dotenv')

const wss = new WebSocket.Server({ port: 8080 });

dotenv.config();
const app = express()
const port = 3003

app.use(express.json())
app.use(cookieParser())
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/messages", (req, res) => {
    res.send("Hello world")
})

app.get("/messages/:idUser", (req, res) => {
    res.send("Hello world")
})

app.listen(port, () => console.log(`Server is listening on port ${port}`))

// Stocker les connexions des utilisateurs
let clients = [];

// Gérer les connexions entrantes
wss.on('connection', (ws) => {
  console.log('Nouvelle connexion établie.');

  // Ajouter le nouveau client à la liste
  clients.push(ws);

  // Événement déclenché lors de la réception de messages
  ws.on('message', (message) => {
    const messageObject = JSON.parse(message);
    console.log('Message reçu :', messageObject);
    
    // Diffuser le message à tous les utilisateurs connectés
    clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify(messageObject));
      }
    });
  });

  // Événement déclenché lorsque la connexion est fermée
  ws.on('close', () => {
    console.log('Connexion fermée.');
    
    // Supprimer le client de la liste
    clients = clients.filter((client) => client !== ws);
  });
});